
require 'nn'

local Maxout, parent = torch.class('nn.Maxout', 'nn.Module')

function Maxout:__init(input, output, k)
   parent.__init(self)
  
   local layer = nn.Sequential()
   local linear = nn.Linear(input, output * k)
   
   layer:add(linear)
   layer:add(nn.Reshape(output, k))
   layer:add(nn.Max(3))
   
   self.modules = layer.modules
      
   self.layer = layer
   self.linear = linear
   
   self.weight = linear.weight
   self.bias = linear.bias
   
end


function Maxout:parameters() 
  return self.layer:parameters()
end

function Maxout:reset(stdv)
  self.linear:reset(stdv)
end

function Maxout:updateOutput(input)
  self.output = self.layer:updateOutput(input)  
  return self.output
end


function Maxout:zeroGradParameters()  
  self.layer:zeroGradParameters() 
end

function Maxout:updateParameters(learningRate)
  self.layer:updateParameters(learningRate)
end

function Maxout:updateGradInput(input, gradOutput)
  self.gradInput = self.layer:updateGradInput(input, gradOutput)
  return self.gradInput
end

function Maxout:accGradParameters(input, gradOutput, scale)
  self.layer:accGradParameters(input, gradOutput, scale) 
end

Maxout.sharedAccUpdateGradParameters = Maxout.accUpdateGradParameters