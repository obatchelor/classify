
local BatchDropout, Parent = torch.class('nn.BatchDropout', 'nn.Module')

function BatchDropout:__init(p)
   Parent.__init(self)
   self.p = p or 0.5
   self.train = true
   -- version 2 scales output during training instead of evaluation
   
   if self.p >= 1 or self.p < 0 then
     error('<BatchDropout> illegal percentage, must be 0 <= p < 1')
   end
   
   self.noise = torch.Tensor()
end

function BatchDropout:updateOutput(input)
   self.output:resizeAs(input):copy(input)
   
   
   if self.train then
     
      local size = input:size()
      size[1] = 1
     
      self.noise:resize(size)
      self.noise = self.noise:expand(input:size())
      
      self.noise:bernoulli(1-self.p)
      self.noise:div(1-self.p)
      
      self.output:cmul(self.noise)
   end
   
   return self.output
end

function BatchDropout:updateGradInput(input, gradOutput)
   if self.train then
      self.gradInput:resizeAs(gradOutput):copy(gradOutput)
      self.gradInput:cmul(self.noise) -- simply mask the gradients with the noise vector
   else
      error('backprop only defined while training')
   end
   return self.gradInput
end

function BatchDropout:setp(p)
   self.p = p
end