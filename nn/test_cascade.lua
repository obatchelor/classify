require 'nn.modules'

local n = nn.Sequential()

local t = nn.CascadeTable()
t:add(nn.Linear(1,2))
-- t:add(nn.Linear(2,3))
-- t:add(nn.Linear(3,7))

n:add(t)
--n:add(nn.Linear(1,5))
n:add(nn.JoinTable(1, 1))


x = torch.Tensor{15}

r = n:forward(x)
print(r)

local g = n:backward(x, r)
print(g)

