local GaussianDropout, Parent = torch.class('nn.GaussianDropout', 'nn.Module')

function GaussianDropout:__init(p)
   Parent.__init(self)
   self.p = p or 0.5
   self.train = true
   
   self.noise = torch.Tensor()
end

function GaussianDropout:updateOutput(input)
   self.output:resizeAs(input):copy(input)
   if self.train then
      self.noise:typeAs(input):resizeAs(input)      
      self.noise:normal(0, self.p)
      self.output:add(self.noise)
   end
   return self.output
end

function GaussianDropout:updateGradInput(input, gradOutput)
   if self.train then
      self.gradInput:resizeAs(gradOutput):copy(gradOutput)
      self.gradInput:cmul(self.noise) -- simply mask the gradients with the noise vector
   else
      error('backprop only defined while training')
   end
   return self.gradInput
end

function GaussianDropout:setp(p)
   self.p = p
end