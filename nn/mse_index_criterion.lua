local MSEIndexCriterion, parent = torch.class('nn.MSEIndexCriterion', 'nn.Criterion')

function MSEIndexCriterion:__init()
   parent.__init(self)
   self.sizeAverage = true
   
   self.target = torch.Tensor()
end

function MSEIndexCriterion:makeTargets(n, labels)
  
  local indices = torch.LongTensor():range(0, labels:size(1) - 1):mul(n)
  indices:add(labels)
  
  self.target:resize(labels:size(1), n):zero()
  self.target:view(self.target:nElement()):indexFill(1, indices, 1)
  
  
  return self.target
end


function MSEIndexCriterion:updateOutput(input, labels) 
 
  local target = self:makeTargets(input:size(2), labels)
  return input.nn.MSECriterion_updateOutput(self, input, target)
end

function MSEIndexCriterion:updateGradInput(input, labels)
  local target = self:makeTargets(input:size(2), labels)
  return input.nn.MSECriterion_updateGradInput(self, input, target)
end


