
require 'classify.modules'
require 'nn'

require 'torch'

local test = {}

function test.module(module, input, parameters, tolerance) 
  
  tolerance = tolerance or 1e-5
  parameters = parameters or {}
  
  local name = torch.type(module)
  
  local jac = nn.Jacobian


  local errJac = jac.testJacobian(module, input) 
  assert (errJac < tolerance, string.format("%s, jacobian test failed, error: %f", name, errJac))
  
  for weight, gradWeight in pairs(parameters) do
  
    local errWeight = jac.testJacobianParameters(module, input, module[weight], module[gradWeight])
    local errUpdWeight = jac.testJacobianParameters(module, input, module[weight], module[gradWeight])
    
    local errAll = jac.testAllUpdate(module, input, weight, gradWeight)

    assert (errWeight < tolerance, string.format("%s.%s, jacobian weight test failed, error: %f", name, weight, errWeight))    
    assert (errUpdWeight < tolerance, string.format("%s.%s, update weight test failed, error: %f", name, weight, errUpdWeight))
  
    for t,err in pairs(errAll) do
      assert (err < tolerance, string.format("%s.%s, update test failed for %s, error: %f", name, weight, t, err))
    end
  end
  

end


function test.cascade(inputs, outputs)

  local total = 0
  local n = {}
  
  local s = nn.Sequential()
  
  local t = nn.CascadeTable()
  
  local last = inputs
  while(outputs - total > 0) do

    local next = math.random(1, (outputs - total))
    t:add(nn.Linear(last, next))
    
    total = total + next
    last = next
  end

  s:add(t)
  s:add(nn.JoinTable(1, 1))
  
  return s
end


function test.modules()
  local r = math.randomseed(os.time())
  local tolerance = tolerance or 1e-5
  
  local n = 10
  
  for i = 1, n do
  
    local inputs = math.random(10,30)
    local outputs = math.random(10,30)
    local batch = math.random(10, 20)
    
    local input = torch.Tensor(batch, inputs)
       
--     test.module(nn.DistanceSq(inputs, outputs), input, {weight = "gradWeight"})
--     test.module(nn.PairwiseDistanceSq(), input)
    
   
    local softmax = nn.Sequential()
    softmax:add(nn.Scale(-1))
    softmax:add(nn.Exp())
    softmax:add(nn.Normalize(2))
    
    test.module(softmax, input)
    
    local cascade = test.cascade(inputs, outputs)
    test.module(cascade, input)
    
    
    test.module(nn.Maxout(inputs, outputs, 2), input)

  end
    
  print (string.format("Passed %d random tests", n))
    
end


test.modules()