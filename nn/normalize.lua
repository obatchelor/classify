
require 'nn'


local Normalize = torch.class('nn.Normalize', 'nn.Module')

function Normalize:__init(dim)
  self.dim = dim or 1
  self.output = torch.Tensor()
  self.gradInput = torch.Tensor()
end
  

function Normalize:updateOutput(input)
  
 
  self.denom = input:sum(self.dim)
  
  
  local size = input:size()
  size[self.dim] = 1
  
  self.output:typeAs(input):resizeAs(input):copy(input)
  self.output:cdiv(self.denom:view(size):expand(input:size()))  
  
  return self.output
  
end

function Normalize:updateGradInput(input, gradOutput)
  
  self.gradInput:typeAs(input):resizeAs(input):fill(0)

  local size = input:size()
  size[self.dim] = 1
  

  
  self.gradInput:copy(gradOutput)
  self.gradInput:cdiv(self.denom:view(size):expand(input:size()))
  
  
  local input_grad = self.gradInput:clone():cmul(self.output)
  input_grad = input_grad:sum(self.dim)
  
  self.gradInput:add(-1, input_grad:view(size):expand(input:size()))  
  
  return self.gradInput
end


local Scale = torch.class('nn.Scale', 'nn.Module')


function Scale:__init(scale)
  self.scale = scale or 1
  self.output = torch.Tensor()
  self.gradInput = torch.Tensor()
end

function Scale:updateOutput(input)
   
  self.output:typeAs(input):resizeAs(input):copy(input)
  self.output:mul(self.scale)
  
  return self.output
end

function Scale:updateGradInput(input, gradOutput)
    self.gradInput:typeAs(input):resizeAs(input):copy(gradOutput)
        
    self.gradInput:div(self.scale)
    return self.gradInput
end



function test() 
  
  require 'nn'

  local tolerance = 1e-5
  
  local jac = nn.Jacobian
  local r = math.randomseed(os.time())
  
  for i = 1, 20 do
    local inputs = math.random(10,20)
    local outputs = math.random(10,20)
    local batch = math.random(10, 20)
    
    local input = torch.Tensor():randn(batch, inputs)
    
    local module = nn.Sequential()
    module:add(nn.Scale(-1))
    module:add(nn.Exp())
    module:add(nn.Normalize(2))
    
--     local module = nn.Normalize(2)
    local err = jac.testJacobian(module, input)
    
    
    assert (err < tolerance, "nn.Normalize, jacobian test failed, error: "..err)
  end
  
end


