
require 'nn'

local Debug, parent = torch.class('nn.Debug', 'nn.Module')

function Debug:__init(forward, backward)
   parent.__init(self)
   
   self.debugForward = forward or print
   self.debugBackward = backward or print

end

function Debug:updateOutput(input)
   self.output = input 
   
   self.debugForward({input = input})
   
   return self.output
end

function Debug:updateGradInput(input, gradOutput)

    self.debugBackward({input = input, gradOutput = gradOutput})

  
   self.gradInput = gradOutput
   return self.gradInput
end