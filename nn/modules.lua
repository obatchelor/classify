require 'classify.flatten'
require 'classify.debug'
require 'classify.maxout'
require 'classify.gaussian_dropout'
require 'classify.batch_dropout'
require 'classify.dropout2'

require 'classify.named_concat_table'

-- require 'classify.distance_sq'
-- require 'classify.pairwise_distance_sq'
require 'classify.normalize'

require 'classify.cascade_table'

require 'classify.pretrained'

require 'classify.class_nll_criterion_2'
require 'classify.mse_index_criterion'