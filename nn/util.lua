
local util = {}


function util.wrapCuda(mlp)
  require 'cunn'
  
  local wrapper = nn.Sequential()  
  
  wrapper:add(nn.Copy('torch.ByteTensor', 'torch.CudaTensor'))
  wrapper:add(mlp:cuda())
  wrapper:add(nn.Copy('torch.CudaTensor', 'torch.FloatTensor'))  
  
  return wrapper
  
end

function util.wrapCudaIn(mlp)
  require 'cunn'
  
  local wrapper = nn.Sequential()  
  
  wrapper:add(nn.Copy('torch.ByteTensor', 'torch.CudaTensor'))
  wrapper:add(mlp:cuda())
  
  return wrapper
  
end

function util.setTrain(mlp, train)
  mlp.train = train
  
  if (mlp.modules) then
    for _, module in pairs(mlp.modules) do
      util.setTrain(module, train)
    end
  end
 
end

function util.setDropout(mlp, p)
  if(type(mlp.p) == "number") then
    mlp.p = p
  end
  
  if (mlp.modules) then
    for _, module in pairs(mlp.modules) do
      util.setDropout(module, p)
    end
  end
 
end


return util