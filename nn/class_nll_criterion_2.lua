local ClassNLLCriterion2, parent = torch.class('nn.ClassNLLCriterion2', 'nn.Criterion')

function ClassNLLCriterion2:__init()
   parent.__init(self)
   self.sizeAverage = true
end

function ClassNLLCriterion2:updateOutput(input, target)
   if input:dim() == 1 then
     
      if(target > 0) then
        self.output = -input[target]
      else
        self.output = 0
      end
      
   elseif input:dim() == 2 then
      local output = 0
      for i=1,target:size(1) do
        
        if(target[i] > 0) then
          output = output - input[i][target[i]]
        end
        
      end
      if self.sizeAverage then
         output = output / target:size(1)
      end
      self.output = output
   else
      error('matrix or vector expected')
   end
   return self.output
end

function ClassNLLCriterion2:updateGradInput(input, target)
   self.gradInput:resizeAs(input)
   self.gradInput:zero()

  if input:dim() == 1 then
    
    if(target > 0) then
      self.gradInput[target] = -1
    end
    
  else
      local z = -1
      if self.sizeAverage then
         z = z / target:size(1)
      end
      local gradInput = self.gradInput
      for i=1,target:size(1) do
         if(target[i] > 0) then
            gradInput[i][target[i]] = z
         end
      end
   end

   return self.gradInput
end