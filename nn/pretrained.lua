 

require 'nn'

local PreTrained, parent = torch.class('nn.PreTrained', 'nn.Module')

function PreTrained:__init(module)
   parent.__init(self)
   self.modules = {module}     
   
   self.module = module
end


function PreTrained:updateOutput(input)
  self.output = self.module:updateOutput(input)  
  return self.output
end


function PreTrained:zeroGradParameters()  
end

function PreTrained:updateParameters(learningRate)
end

function PreTrained:updateGradInput(input, gradOutput)
   self.gradInput:zero():resizeAs(input)
   return self.gradInput
end

function PreTrained:accGradParameters(input, gradOutput, scale)
end

PreTrained.sharedAccUpdateGradParameters = PreTrained.accUpdateGradParameters