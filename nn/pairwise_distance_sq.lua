require 'nn'
local metrics = require 'metrics'

local PairwiseDistanceSq, parent = torch.class('nn.PairwiseDistanceSq', 'nn.Module')

function PairwiseDistanceSq:__init()
   parent.__init(self)

   self.gradInput = torch.Tensor():zero()
   self.output = torch.Tensor():zero()
   
   
   self:reset()
end




function PairwiseDistanceSq:updateOutput(input)
  self.output = metrics.distancesL2 { query = input, ref = input }
  
--   print {input, self.output}
  return self.output
end


local function gradient(input, gradOutput) 
  
  local gradInput = torch.Tensor():typeAs(input):resize(input:size()):zero()  
  local g  = gradOutput:sum(2):expand(input:size())

  gradInput:addcmul(2, input, g)  
  gradInput:addmm(-2, gradOutput, input) 
  
  return gradInput
end


function PairwiseDistanceSq:updateGradInput(input, gradOutput)
  
  self:updateOutput(input)
  
  self.gradInput = gradient(input, gradOutput:t())
  self.gradInput:add(gradient(input, gradOutput))
  
  return self.gradInput
end




