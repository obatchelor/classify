require 'nn'

local metrics = require 'metrics'

local DistanceSq, parent = torch.class('nn.DistanceSq', 'nn.Module')

function DistanceSq:__init(inputs, outputs)
   parent.__init(self)

   self.weight = torch.Tensor(outputs, inputs):zero()
   self.gradWeight = torch.Tensor(outputs, inputs):zero()
   
   self.weightCopy = torch.Tensor(outputs, inputs):zero() 

   self.gradInput = torch.Tensor():zero()
   self.output = torch.Tensor():zero()
   
   
   self.inputs = inputs
   self.outputs = outputs

   self:reset()
end




function DistanceSq:reset(stdv)
   if stdv then
      stdv = stdv * math.sqrt(3)
   else
      stdv = 1./math.sqrt(self.weight:size(1))
   end
   self.weight:uniform(-stdv, stdv)
end




function DistanceSq:updateOutput(input)
--   local batch = input:size(1)
--   
--   self.output:typeAs(input):resize(batch, self.outputs):zero()
--   self.output:addmm(-2, input, self.weight:t())  
--   
--   local weightSq = self.weight:clone():cmul(self.weight)
--   local inputSq = input:clone():cmul(input)
--   
--   local inputSq = inputSq:sum(2):view(batch, 1):expand(batch, self.outputs)
--   local weightSq = weightSq:sum(2):view(1, self.outputs):expand(batch, self.outputs)
--   
--   self.output:add(inputSq)
--   self.output:add(weightSq)
  self.output = metrics.distancesL2 { query = self.weight, ref = input }
  return self.output   

end

function DistanceSq:updateGradInput(input, gradOutput)
  
--   self:updateOutput(input)
  
  local nElement = self.gradInput:nElement()
  self.gradInput:resizeAs(input):zero()
  
  
  self.gradInput:typeAs(input):resizeAs(input)
  
  local g  = gradOutput:sum(2):expand(input:size())

  self.gradInput:addcmul(2, input, g)  
  self.gradInput:addmm(-2, gradOutput, self.weight)
  
  self.weightCopy:copy(self.weight)
  
  return self.gradInput
end

function DistanceSq:accGradParameters(input, gradOutput, scale)
  local scale = scale or 1
--   
  local gradOutput = gradOutput:t()  
  local g  = gradOutput:sum(2):expand(self.weight:size())

  self.gradWeight:addcmul(scale * 2, self.weightCopy, g)  
  self.gradWeight:addmm(-scale * 2, gradOutput, input)

end



DistanceSq.sharedAccUpdateGradParameters = DistanceSq.accUpdateGradParameters

function test() 
  
  require 'nn'
  
  local jac = nn.Jacobian
  local r = math.randomseed(os.time())

  for i = 1,10 do
    local inputs = math.random(10,20)
    local outputs = math.random(10,20)
    local batch = math.random(10, 20)
    
    
    local input = torch.Tensor(batch, inputs):zero()
    
    local module = nn.DistanceSq(inputs, outputs)
    local errJac = jac.testJacobian(module, input) 
    
    local errWeight = jac.testJacobianParameters(module, input, module.weight, module.gradWeight)
    local errUpdWeight = jac.testJacobianParameters(module, input, module.weight, module.gradWeight)
    
    local errAll = jac.testAllUpdate(module, input, 'weight', 'gradWeight')

    local tolerance = 1e-5
    assert (errJac < tolerance, "nn.DistanceSq, jacobian test failed, error: "..errJac)
    
    assert (errWeight < tolerance, "nn.DistanceSq, jacobian test failed, error: "..errJac)
    assert (errUpdWeight < tolerance, "nn.DistanceSq, jacobian test failed, error: "..errJac)
    
    for t,err in pairs(errAll) do
      assert (err < tolerance, "nn.DistanceSq, "..t.." test failed, error: "..err)
    end
  end

end


