local NamedConcatTable, parent = torch.class('nn.NamedConcatTable', 'nn.Module')

function NamedConcatTable:__init()
   parent.__init(self)
   self.modules = {}
   self.namedModules = {}
   
   self.output = {}
end

function NamedConcatTable:add(name, module)
   table.insert(self.modules, module)
   self.namedModules[name] = module
   
   return self
end

function NamedConcatTable:get(index)
   return self.modules[index]
end

function NamedConcatTable:size()
   return #self.modules 
end

function NamedConcatTable:updateOutput(input)
   for k, module in pairs(self.namedModules) do
      self.output[k] = module:updateOutput(input)
   end
   return self.output
end

function NamedConcatTable:updateGradInput(input, gradOutput)
   local first = true
   
   for k, module in pairs(self.namedModules) do
     
      local currentGradInput = module:updateGradInput(input, gradOutput[k])
      if first then
         self.gradInput:resizeAs(currentGradInput):copy(currentGradInput)
         first = false
      else
         self.gradInput:add(currentGradInput)
      end
   end
   return self.gradInput
end

function NamedConcatTable:accGradParameters(input, gradOutput, scale)
   scale = scale or 1
   for k, module in pairs(self.namedModules) do
      module:accGradParameters(input, gradOutput[k], scale)
   end
end

function NamedConcatTable:accUpdateGradParameters(input, gradOutput, lr)
   for k, module in pairs(self.namedModules) do
      module:accUpdateGradParameters(input, gradOutput[k], lr)
   end
end

function NamedConcatTable:zeroGradParameters()
   for _, module in pairs(self.namedModules) do
      module:zeroGradParameters()
   end
end

function NamedConcatTable:updateParameters(learningRate)
   for _, module in pairs(self.namedModules) do
      module:updateParameters(learningRate)
   end
end

function NamedConcatTable:share(mlp,...)
   for k, module in pairs(self.namedModules) do
      module:share(mlp.modules[k],...); 
   end
end

function NamedConcatTable:parameters()
   local function tinsert(to, from)
      if type(from) == 'table' then
         for i=1,#from do
            tinsert(to,from[i])
         end
      else
         table.insert(to,from)
      end
   end
   local w = {}
   local gw = {}
   for i=1,#self.modules do
      local mw,mgw = self.modules[i]:parameters()
      if mw then
         tinsert(w,mw)
         tinsert(gw,mgw)
      end
   end
   return w,gw
end

function NamedConcatTable:__tostring__()
   local tab = '  '
   local line = '\n'
   local next = '  |`-> '
   local ext = '  |    '
   local extlast = '       '
   local last = '   ... -> '
   local str = 'nn.NamedConcatTable'
   str = str .. ' {' .. line .. tab .. 'input'
   for k, module in pairs(self.namedModules) do
      if k == self.modules then
         str = str .. line .. tab .. next .. '(' .. k .. '): ' .. tostring(module):gsub(line, line .. tab .. extlast)
      else
         str = str .. line .. tab .. next .. '(' .. k .. '): ' .. tostring(module):gsub(line, line .. tab .. ext)
      end
   end
   str = str .. line .. tab .. last .. 'output'
   str = str .. line .. '}'
   return str
end