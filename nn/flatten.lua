
require 'nn'

local Flatten, parent = torch.class('nn.Flatten', 'nn.Module')

function Flatten:__init(...)
   parent.__init(self)

end

function Flatten:updateOutput(input)
  assert(input:dim() > 1, "input must have at least batchxsize dimensions") 
  
  
  input = input:contiguous()
   
   local size = input:size()
   local elems = 1
      
   for i = 2, #size do
     elems = elems * size[i]
   end

   self.output:set(input):resize(input:size(1), elems)
   return self.output
end

function Flatten:updateGradInput(input, gradOutput)
   gradOutput = gradOutput:contiguous()
   self.gradInput:set(gradOutput):resizeAs(input)
   return self.gradInput
end


