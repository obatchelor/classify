local Dropout2, Parent = torch.class('nn.Dropout2', 'nn.Module')

function Dropout2:__init(p)
   Parent.__init(self)
   self.p = p or 0.5
   self.train = true

   if self.p >= 1 or self.p < 0 then
      error('<Dropout> illegal percentage, must be 0 <= p < 1')
   end
   self.noise = torch.Tensor()
   
end

function Dropout2:updateOutput(input)
   
   self.output:resizeAs(input):copy(input)
   if self.train then
      self.noise:resizeAs(input)
      self.noise:bernoulli(1-self.p)
      self.noise:div(1-self.p)
      self.output:cmul(self.noise)
   end
   return self.output
end

function Dropout2:updateGradInput(input, gradOutput)
   if self.train then
      self.gradInput:resizeAs(gradOutput):copy(gradOutput)
      self.gradInput:cmul(self.noise) -- simply mask the gradients with the noise vector
   else
      error('backprop only defined while training')
   end
   return self.gradInput
end

function Dropout2:setp(p)
   self.p = p
end