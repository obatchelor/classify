
require 'nn'

local CascadeTable, parent = torch.class('nn.CascadeTable', 'nn.Module')

function CascadeTable:__init(input, output, k)
  parent.__init(self)
  
  local layer = nn.Sequential() 
  self.modules = {layer}
  
  self.layer = layer
end


function CascadeTable:add(module)
  self.layer:add(module)
  return self
end

function CascadeTable:size()
  return self.layer:size()
end

function CascadeTable:get(index)
  return self.layer:get(index)
end


function CascadeTable:parameters() 
  return self.layer:parameters()
end

function CascadeTable:reset(stdv)
  self.layer:reset(stdv)
end

function CascadeTable:updateOutput(input)
  self.layer:updateOutput(input)  
  self.output = {}
  
  for i = 1, self.layer:size() do
    table.insert(self.output, self.layer:get(i).output)
  end
  
  return self.output
end


function CascadeTable:zeroGradParameters()  
  self.layer:zeroGradParameters() 
  
end

function CascadeTable:updateParameters(learningRate)
  self.layer:updateParameters(learningRate)
end

function CascadeTable:updateGradInput(input, gradOutput)
     
  local size = self.layer:size()
  
  local currentGradOutput = gradOutput[size]
  local currentModule = self.layer:get(size)
    
  
  for i = size-1, 1, -1 do
    local previousModule = self.layer:get(i)
    
    currentGradOutput = currentModule:updateGradInput(previousModule.output, currentGradOutput):clone()
    currentGradOutput:add(gradOutput[i])
    
    currentModule = previousModule
  end
  
    
  currentGradOutput = currentModule:updateGradInput(input, currentGradOutput)
  
  self.gradInput = currentGradOutput
  return currentGradOutput
end

function CascadeTable:accGradParameters(input, gradOutput, scale)
  self.layer:accGradParameters(input, gradOutput[self.layer:size()], scale) 
end

CascadeTable.sharedAccUpdateGradParameters = CascadeTable.accUpdateGradParameters





