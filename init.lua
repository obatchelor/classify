require 'nn'
require 'torch'

require 'nnx'
require 'classify.modules'


classify = {}


function classify.load(storage)
  
  
  local file = torch.MemoryFile(storage, "r")
  file['binary'](file)
  
  local model = file:readObject()
  
  file:close()
 

  print("loaded model: ")
  print(model)
  
  if(model) then
    classify.model = model
    return true
  end
    
  
    
  return false
  
end


function eqSize(s1, s2)
  if(s1:size() ~= s2:size()) then
    return false
  end
  
  for i = 1, s1:size() do
    if(s1[i] ~= s2[i]) then
      return false
    end
  end
  
  return true
end

function classify.classify(image)
  
  assert(classify.model, "classify: model not initialized")
  local model = classify.model
  local imageSize = model.imageSize
    
  assert(eqSize(torch.LongStorage (imageSize), image:size()), "classify: provided image has incorrect size")
  local batch = image:view(1, image:size(1), image:size(2), image:size(3))

  
  local output = model:forward(batch:float()) 
  local index, confidence = output:squeeze():max(1)
  
  return confidence[1], index[1]
  
end


return classify

